# Ungit on i #

Complementary tools for [Ungit](https://github.com/FredrikNoren/ungit) on IBM i.

### What is this repository for? ###

This repository contains tools useful when Ungit is installed on the IBM i. Ungit is a node.js applikation, and starting and stopping the Ungit server job can be a bit difficult without these tools. 

#### STRUNGIT - start the Ungit server for a user ####

Ungit is a single user application so more instances of Ungit must be started to serve multiple users. This command will start a server job for the user specified in the user profile parameter. The default value is *CURRENT meaning the server job will be started for the user issuing the command.

STRUNGIT expects to find the configuration for the Ungit server job in a data area called UNGIT in a library with the same name as the user profile. If not found, an error message will be issued. To set up a user for Ungit, please enter the following commands:

```
CRTLIB <user>
CRTDTAARA DTAARA(<user>/UNGIT) TYPE(*CHAR) LEN(8) TEXT('Ungit server config')
CHGDTAARA DTAARA(<user>/UNGIT (1 5)) VALUE('<port number')
CHGDTAARA DTAARA(<user>/UNGIT (7 2)) VALUE('run priority')
```

Now the STRUNGIT will start the Ungit server job on the port and with the run priority specified in the UNGIT data area.

STRUNGIT submits the Ungit server job to job queue QSYSNOMAX, so the server job will run in subsystem QSYSWRK. The job run priority feature changes the routing data on the submit to "RUNPTY" + the run priority value. If no run priority is defined in the Ungit configuration data area, run priority 50 will be used.

The subsystem QSYSWRK has the following routing data compare values by default: RUNPTY07, RUNPTY10, RUNPTY20, RUNPTY25, RUNPTY35, RUNPTY40 and RUNPTY50. So run priorities 07, 10, 20, 25, 35, 40 and 50 will work without any additional work management configuration.

The STRUNGIT command has a parameter OPTIONS where you can enter extra options used when starting the Ungit server job. For additional information about these options, check the file [config.js](https://https://github.com/FredrikNoren/ungit/blob/master/source/config.js) in the Ungit repository.

Note: You should not enter "--port" and "--ungitBindIp" in the OPTIONS parameter, these are used internally by the STRUNGIT command.

#### ENDUNGIT - end the Ungit server ####

ENDUNGIT will end the Ungit server job for either the current user, any user or one specified user.