/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/*  Start Ungit Server.                                                                           */
/*                                                                                                */
/*  Build instructions:                                                                           */
/*                                                                                                */
/*> CRTFRMSTMF OBJ(&FCN2/&FNR) CMD(CRTCMD) SRCSTMF('&FP') PARMS('PGM(&FCN2/&FNR) HLPID(*CMD)   - <*/
/*>              HLPPNLGRP(&FCN2/&FNR) PRDLIB(&FCN2)')                                           <*/
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/
/*  2016-06-30 Christian Jorgensen                                                                */
/*             Create command.                                                                    */
/*  2017-03-22 Christian Jorgensen                                                                */
/*             Move source to IFS.                                                                */
/*  2017-03-28 Christian Jorgensen                                                                */
/*             Add parameter for user profile.                                                    */
/*  2019-02-19 Christian Jorgensen                                                                */
/*             Add parameter for Ungit options.                                                   */
/*------------------------------------------------------------------------------------------------*/

cmd        prompt('Start Ungit Server') text(*cmdpmt)

parm       USRPRF     *name   10  dft(*CURRENT) spcval((*CURRENT))               prompt('User profile')
parm       OPTIONS    *char 4000  dft(*NONE)    spcval((*NONE '')) inlpmtlen(32) prompt('Ungit options')
