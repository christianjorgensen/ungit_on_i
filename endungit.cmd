/*-------------------------------------------------------------------*/
/*                                                                   */
/*  End Ungit Server.                                                */
/*                                                                   */
/*  Build instructions:                                              */
/*                                                                   */
/*> CRTFRMSTMF OBJ(&FCN2/&FNR) CMD(CRTCMD) SRCSTMF('&FP')         - <*/
/*>               PARMS('PGM(&FCN2/&FNR) HLPID(*CMD)              - <*/
/*>                      HLPPNLGRP(&FCN2/&FNR) PRDLIB(&FCN2)')      <*/
/*                                                                   */
/*-------------------------------------------------------------------*/
/*  2016-06-30 Christian Jorgensen                                   */
/*             Create command.                                       */
/*  2017-03-22 Christian Jorgensen                                   */
/*             Move source to IFS.                                   */
/*  2017-10-30 Christian Jorgensen                                   */
/*             Add parameter for user profile.                       */
/*-------------------------------------------------------------------*/

cmd        prompt('End Ungit Server') text(*cmdpmt)

parm       USRPRF     *name  10  dft(*CURRENT) spcval((*CURRENT) (*ALL)) prompt('User profile')
